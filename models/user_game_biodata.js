"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User_game_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  User_game_biodata.init(
    {
      uuid: {
        type: DataTypes.STRING,
        defaultValue: DataTypes.UUIDV4,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: { msg: "user must have a name" },
          notEmpty: { msg: "name must not be empty" },
        },
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: { msg: "user must have a email" },
          notEmpty: { msg: "email must not be empty" },
          isEmail: { msg: "must be a valid email" },
        },
      },
      gender: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: { msg: "user must have a gender" },
          notEmpty: { msg: "gender must not be empty" },
        },
      },
      date_of_birth: {
        type: DataTypes.STRING,
      },
      city: { type: DataTypes.STRING },
    },
    {
      sequelize,
      tableName: "user_game_biodata",
      modelName: "User_game_biodata",
    }
  );
  return User_game_biodata;
};
