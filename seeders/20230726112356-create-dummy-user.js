"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "user_games",
      [
        {
          uuid: "169cd74e-e72c-4dcf-b2e7-46d5b36b4d91",
          username: "okakrisna",
          password: "sgsgs",
          role: "admin",
          createdAt: "2023-07-19T07:27:43.155Z",
          updatedAt: "2023-07-19T08:49:52.097Z",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
