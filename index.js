// import module
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const fs = require("fs");
const { sequelize, User_game } = require("./models");

// use middleware
app.use(bodyParser.urlencoded({ extends: false }));

// use view engine
app.use(express.static("public"));
app.use(express.json());
app.set("view engine", "ejs");

// middleware login to page landingpage
const authenticateUser = (req, res, next) => {
  const { email, password } = req.body;

  // read file users.json
  const userData = JSON.parse(fs.readFileSync("users.json"));

  // find user email and password user in users.json
  const user = userData.users.find(
    (user) => user.email === email && user.password === password
  );
  // if statement, if user can pass middleware
  if (user) {
    next();
  } else {
    res.render("login-wrongID", { textErr: "Incorrect Email or Password" });
  }
};

// middleware input
app.post("/login", authenticateUser, (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  res.redirect("/");
});

// routing to the /
app.get("/", (req, res) => {
  res.render("landing-page");
});

// routing to the /work
app.get("/work", (req, res) => {
  res.render("work");
});

// routing to the /contact
app.get("/contact", (req, res) => {
  res.render("contact");
});

//routing to the /aboutme
app.get("/aboutme", (req, res) => {
  res.render("aboutme");
});

//routing to the /top-scores
app.get("/top-scores", (req, res) => {
  res.render("top-scores");
});

//routing to the /news
app.get("/news", (req, res) => {
  res.render("news");
});

//routing to the /login
app.get("/login", (req, res) => {
  res.render("login");
});
app.get("/sign-up", (req, res) => {
  res.render("sign-up");
});

//routing to the /game-suit
app.get("/game-suit", (req, res) => {
  res.render("game-suit");
});

//! ch 6

const authenticateAdmin = (req, res, next) => {
  const { username, password } = req.body;

  const adminData = JSON.parse(fs.readFileSync("admin.json"));

  const admin = adminData.admin.find(
    (admin) => admin.username === username && admin.password === password
  );
  if (admin) {
    next();
  } else {
    res.render("login-wrongID", { textErr: "Incorrect Username or Password" });
  }
};

app.get("/login-admin", (req, res) => {
  res.render("admin/login-admin");
});

app.get("/user-game-history", (req, res) => {
  res.render("admin/user-game-history");
});

app.post("/login-admin", authenticateAdmin, (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  res.redirect("/dashboard");
});

app.get("/dashboard", (req, res) => {
  User_game.findAll().then((users) => {
    res.render("admin/dashboard", {
      users,
    });
  });
});

app.get("/create", (req, res) => {
  res.render("admin/create-user");
});

app.get("/user-game-biodata", (req, res) => {
  res.render("admin/user-game-biodata");
});

app.post("/create", (req, res) => {
  User_game.create({
    username: req.body.username,
    email: req.body.email,
    password: req.body.password,
    gender: req.body.gender,
    role: req.body.role,
    date: req.body.date,
    city: req.body.city,
  }).then((users) => {
    res.redirect("/dashboard");
  });
});

app.post("/delete/:id", (req, res) => {
  const id = req.params.id;
  User_game.destroy({
    where: { id: id },
  })
    .then(() => {
      res.redirect("/dashboard");
    })
    .catch((err) => {
      console.error("Error deleting user: ", err);
      res.status(500).send("Error deleting user");
    });
});

app.post("/delete-all", async (req, res) => {
  try {
    await User_game.destroy({
      where: {},
      truncate: true,
    });

    // Reset auto increment
    await sequelize.query("ALTER TABLE User_game AUTO_INCREMENT = 1");

    res.redirect("/dashboard");
  } catch (err) {
    console.error("Error deleting all users: ", err);
    res.status(500).send("Error deleting all users");
  }
});

app.get("/update/:id", (req, res) => {
  User_game.findOne({
    where: { id: req.params.id },
  }).then((user) => {
    res.render("admin/update", {
      id: user.id,
      username: user.username,
      email: user.email,
      password: user.password,
      gender: user.gender,
      role: user.role,
      date: user.date,
      city: user.city,
    });
  });
});

app.post("/update/:id", (req, res) => {
  User_game.update(
    {
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      gender: req.body.gender,
      role: req.body.role,
      date: req.body.date,
      city: req.body.city,
    },
    {
      where: { id: req.params.id },
    }
  )
    .then((users) => {
      res.redirect("/dashboard");
    })
    .catch((err) => {
      console.error("Error updating user: ", err);
      res.status(500).send("Error updating user");
    });
});

// turnon the server on http://localhodt:3000
app.listen(3000, () => {
  console.log("server on http://localhost:3000");
});
